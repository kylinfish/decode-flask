Create VirtualEnv:
------------------
virtualenv Flask-Env


Install Flask:
--------------
ln -s [your-path]/decode-flask/Jinja2-2.7.3/jinja2/ ./Flask-Env/lib/python2.7/site-packages/jinja2
ln -s [your-path]/decode-flask/Werkzeug-0.10.1/werkzeug/ ./Flask-Env/lib/python2.7/site-packages/werkzeug
ln -s [your-path]/decode-flask/Flask-0.10.1/flask/ ./Flask-Env/lib/python2.7/site-packages/flask

ln -s [your-path]/decode-flask/Flask-Admin-1.1.0/flask_admin/ ./Flask-Env/lib/python2.7/site-packages/flask_admin
ln -s [your-path]/decode-flask/Flask-Mail-0.9.1/flask_mail.py ./Flask-Env/lib/python2.7/site-packages/flask_mail.py

Start Python VirtualEnv:
------------------------
source ./Flask-Env/bin/activate
pip install -r requirements.txt


Change File Mode:
-----------------
sudo chmod -R 755 ../
