Support Chinese Note:
=====================
::

    #!usr/bin/env python
    # coding: utf-8


Decode Note Sign:
=================
::

    # KY: Here is My Add Note.


Build Script  For Docs:
=======================
::

    sphinx-build -Q -b html ./Flask-0.10.1/docs ./Flask-0.10.1/_build_docs/
    sphinx-build -Q -b html ./Jinja2-2.7.3/docs ./Jinja2-2.7.3/_build_docs/
    sphinx-build -Q -b html ./Werkzeug-0.10.1/docs ./Werkzeug-0.10.1/_build_docs/
    sphinx-build -Q -b html ./Flask-Mail-0.9.1/docs ./Flask-Mail-0.9.1/_build_docs/


Flask_Docs_Zh_Cn:
=================

http://docs.jinkan.org/docs/flask/
http://dormousehole.readthedocs.org/en/latest/
https://github.com/dormouse/Flask_Docs_ZhCn.git

http://www.hustlzp.com/post/2014/08/flask-config

Jinja2_Docs_Zh_Cn:
==================

http://docs.jinkan.org/docs/jinja2/
http://erhuabushuo.is-programmer.com/posts/33926.html

About Dependency:
=================
add packages:
Jinja2==2.7.3
Werkzeug==0.10.1

Build Docs Error:
=================
werkzeug Error:
    Theme error: no theme named 'werkzeug' found (missing theme.conf?)

jinja2 Error:
    Extension error: Could not import extension jinjaext (exception: cannot import name next)
