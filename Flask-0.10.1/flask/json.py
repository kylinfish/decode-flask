# -*- coding: utf-8 -*-
"""
    flask.jsonimpl
    ~~~~~~~~~~~~~~

    Implementation helpers for the JSON support in Flask.

    :copyright: (c) 2012 by Armin Ronacher.
    :license: BSD, see LICENSE for more details.
"""
# KY: 在Flask里实现对JSON支持的封装.

import io
import uuid
from datetime import datetime
from .globals import current_app, request
from ._compat import text_type, PY2

from werkzeug.http import http_date
from jinja2 import Markup

# Use the same json implementation as itsdangerous on which we
# depend anyways.

# KY: 以itsdangerous我们赖以的方式, 来实现相同的json.
try:
    from itsdangerous import simplejson as _json
except ImportError:
    from itsdangerous import json as _json


# figure out if simplejson escapes slashes.  This behavior was changed
# from one version to another without reason.

# KY: 标记simplejson转义斜杠, 这种行为没有理由, 它在从一个版本到另一个中被改变.
_slash_escape = '\\/' not in _json.dumps('/')

__all__ = ['dump', 'dumps', 'load', 'loads', 'htmlsafe_dump',
           'htmlsafe_dumps', 'JSONDecoder', 'JSONEncoder',
           'jsonify']


def _wrap_reader_for_text(fp, encoding):
    # KY: 为读数据做处理.
    if isinstance(fp.read(0), bytes):
        fp = io.TextIOWrapper(io.BufferedReader(fp), encoding)
    return fp


def _wrap_writer_for_text(fp, encoding):
    # KY: 为写数据做处理.
    try:
        fp.write('')
    except TypeError:
        fp = io.TextIOWrapper(fp, encoding)
    return fp


class JSONEncoder(_json.JSONEncoder):
    """The default Flask JSON encoder.  This one extends the default simplejson
    encoder by also supporting ``datetime`` objects, ``UUID`` as well as
    ``Markup`` objects which are serialized as RFC 822 datetime strings (same
    as the HTTP date format).  In order to support more data types override the
    :meth:`default` method.
    """

    # KY: 默认的Flask JSON encoder. 它扩展了默认的encoder, 还支持时间类型对象, 'UUID' 以及
    # 'Markup' 被序列化的对象, RFC 822 datetime字符串(HTTP日期格式也一样), 复写 'default'
    # 方法以为支持更多的类型.

    def default(self, o):
        """Implement this method in a subclass such that it returns a
        serializable object for ``o``, or calls the base implementation (to
        raise a ``TypeError``).

        For example, to support arbitrary iterators, you could implement
        default like this::

            def default(self, o):
                try:
                    iterable = iter(o)
                except TypeError:
                    pass
                else:
                    return list(iterable)
                return JSONEncoder.default(self, o)
        """
        # KY: 在子类中实现该方法, 它返回一个可序列化的对象为'o', 或调用基实现(抛出一个'typeError').
        # 例如, 为支持任意的迭代器, 你可以实现这样的 'default'.

        if isinstance(o, datetime):
            return http_date(o)
        if isinstance(o, uuid.UUID):
            return str(o)
        if hasattr(o, '__html__'):
            return text_type(o.__html__())
        return _json.JSONEncoder.default(self, o)


class JSONDecoder(_json.JSONDecoder):
    """The default JSON decoder.  This one does not change the behavior from
    the default simplejson encoder.  Consult the :mod:`json` documentation
    for more information.  This decoder is not only used for the load
    functions of this module but also :attr:`~flask.Request`.
    """
    # KY: 默认的JSON decoder. 它不会改变来自默认decoder的行为.


def _dump_arg_defaults(kwargs):
    """Inject default arguments for dump functions."""
    # KY: 为dump functions 注入的默认参数.

    if current_app:
        kwargs.setdefault('cls', current_app.json_encoder)
        if not current_app.config['JSON_AS_ASCII']:
            kwargs.setdefault('ensure_ascii', False)
        kwargs.setdefault('sort_keys', current_app.config['JSON_SORT_KEYS'])
    else:
        kwargs.setdefault('sort_keys', True)
        kwargs.setdefault('cls', JSONEncoder)


def _load_arg_defaults(kwargs):
    """Inject default arguments for load functions."""
    # KY: 为load functions 注入的默认参数.

    if current_app:
        kwargs.setdefault('cls', current_app.json_decoder)
    else:
        kwargs.setdefault('cls', JSONDecoder)


def dumps(obj, **kwargs):
    """Serialize ``obj`` to a JSON formatted ``str`` by using the application's
    configured encoder (:attr:`~flask.Flask.json_encoder`) if there is an
    application on the stack.

    This function can return ``unicode`` strings or ascii-only bytestrings by
    default which coerce into unicode strings automatically.  That behavior by
    default is controlled by the ``JSON_AS_ASCII`` configuration variable
    and can be overriden by the simplejson ``ensure_ascii`` parameter.
    """
    # KY: 在堆栈上, 如果有一个应用程序, 使用应用程序配置的 encoder 序列化 'obj' 为一个JSON格式的 'str'.
    # 这个函数可以返回 'unicode' 字符串或ascii-only byte strings, 默认自动强制为unicode字符串.
    # 默认行为是由'JSON_AS_ASCII'配置变量控制, 被 simplejson 'ensure_ascii'参数覆载.

    _dump_arg_defaults(kwargs)
    encoding = kwargs.pop('encoding', None)
    rv = _json.dumps(obj, **kwargs)
    if encoding is not None and isinstance(rv, text_type):
        rv = rv.encode(encoding)
    return rv


def dump(obj, fp, **kwargs):
    """Like :func:`dumps` but writes into a file object."""
    # KY: 如同函数 'dumps', 但写入到一个文件对象中.

    _dump_arg_defaults(kwargs)
    encoding = kwargs.pop('encoding', None)
    if encoding is not None:
        fp = _wrap_writer_for_text(fp, encoding)
    _json.dump(obj, fp, **kwargs)


def loads(s, **kwargs):
    """Unserialize a JSON object from a string ``s`` by using the application's
    configured decoder (:attr:`~flask.Flask.json_decoder`) if there is an
    application on the stack.
    """
    # KY: 在堆栈上, 如果有一个应用程序, 使用应用程序配置的 decoder 从一个字符串's'逆序列化一个JSON对象.

    _load_arg_defaults(kwargs)
    if isinstance(s, bytes):
        s = s.decode(kwargs.pop('encoding', None) or 'utf-8')
    return _json.loads(s, **kwargs)


def load(fp, **kwargs):
    """Like :func:`loads` but reads from a file object.
    """
    # KY: 如同函数 'loads', 但从一个文件对象中读.

    _load_arg_defaults(kwargs)
    if not PY2:
        fp = _wrap_reader_for_text(fp, kwargs.pop('encoding', None) or 'utf-8')
    return _json.load(fp, **kwargs)


def htmlsafe_dumps(obj, **kwargs):
    """Works exactly like :func:`dumps` but is safe for use in ``<script>``
    tags.  It accepts the same arguments and returns a JSON string.  Note that
    this is available in templates through the ``|tojson`` filter which will
    also mark the result as safe.  Due to how this function escapes certain
    characters this is safe even if used outside of ``<script>`` tags.

    The following characters are escaped in strings:

    -   ``<``
    -   ``>``
    -   ``&``
    -   ``'``

    This makes it safe to embed such strings in any place in HTML with the
    notable exception of double quoted attributes.  In that case single
    quote your attributes or HTML escape it in addition.

    .. versionchanged:: 0.10
       This function's return value is now always safe for HTML usage, even
       if outside of script tags or if used in XHTML.  This rule does not
       hold true when using this function in HTML attributes that are double
       quoted.  Always single quote attributes if you use the ``|tojson``
       filter.  Alternatively use ``|tojson|forceescape``.
    """
    # KY: 如同函数 'dumps', 但对'<script>'标签是安全的, 它接受一些相同的参数, 返回
    # 一个JSON 字符串. 注意, 在模板中这是变量, 通过'|tojson' 过滤器将结果标记为安全.
    # 这归于这个函数如何转义某些字符使它安全, 即使'<script>'标签以外使用.

    # KY: 以下这些字符在字符串里将被转义:

    # KY: 这使嵌入在HTML任何地方的字符串安全, 双引用引用属性例外.
    # 在这种情况下, 单引号属性或HTML 转义它.

    # KY: version changed 0.10
    # 这个函数为使用HTML返回总是安全的值, 即使在脚本标记之外或者用于XHTML.
    # 在HTML中双引号引用属性时使用这个函数, 这条规则并不适用.
    # 你如果使用'|tojson'过滤器, 要总是单引用属性, 或者使用'|tojson|forceescape'.

    rv = dumps(obj, **kwargs) \
        .replace(u'<', u'\\u003c') \
        .replace(u'>', u'\\u003e') \
        .replace(u'&', u'\\u0026') \
        .replace(u"'", u'\\u0027')
    if not _slash_escape:
        rv = rv.replace('\\/', '/')
    return rv


def htmlsafe_dump(obj, fp, **kwargs):
    """Like :func:`htmlsafe_dumps` but writes into a file object.
    """
    # KY: 如同函数 'htmlsafe_dumps', 但写入到一个文件对象.

    fp.write(unicode(htmlsafe_dumps(obj, **kwargs)))


def jsonify(*args, **kwargs):
    """Creates a :class:`~flask.Response` with the JSON representation of
    the given arguments with an `application/json` mimetype.  The arguments
    to this function are the same as to the :class:`dict` constructor.

    Example usage::

        from flask import jsonify

        @app.route('/_get_current_user')
        def get_current_user():
            return jsonify(username=g.user.username,
                           email=g.user.email,
                           id=g.user.id)

    This will send a JSON response like this to the browser::

        {
            "username": "admin",
            "email": "admin@localhost",
            "id": 42
        }

    For security reasons only objects are supported toplevel.  For more
    information about this, have a look at :ref:`json-security`.

    This function's response will be pretty printed if it was not requested
    with ``X-Requested-With: XMLHttpRequest`` to simplify debugging unless
    the ``JSONIFY_PRETTYPRINT_REGULAR`` config parameter is set to false.
    .. versionadded:: 0.2
    """
    # KY: 用给定参数的JSON表示, 一个 'application/JSON' mimetype, 创建
    # 一个 'flask.Response' 类. 这个函数的参数和 'dict' 的构造器一样.

    # KY: 使用实例.
    # KY: 这将发送像这样一个JSON响应给浏览器.

    # KY: 出于安全考虑, 只支持顶层的对象. 关于这个的更多信息, 看一下 'json-security' 参考.
    # 这个函数的响应将会很美丽地显示, 如果不要求'X-Requested-With:XMLHttpRequest' 的简化调试,
    # 除非'JSONIFY_PRETTYPRINT_REGULAR'配置参数设置为false.

    indent = None
    if current_app.config['JSONIFY_PRETTYPRINT_REGULAR'] \
            and not request.is_xhr:
        indent = 2
    return current_app.response_class(dumps(dict(*args, **kwargs),
                                            indent=indent),
                                      mimetype='application/json')


def tojson_filter(obj, **kwargs):
    return Markup(htmlsafe_dumps(obj, **kwargs))
