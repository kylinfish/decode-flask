#!usr/bin/env python
# coding: utf-8

"""
    flask.config
    ~~~~~~~~~~~~

    Implements the configuration related objects.

    :copyright: (c) 2011 by Armin Ronacher.
    :license: BSD, see LICENSE for more details.
"""

# KY: 实现了配置相关对象.

import imp
import os
import errno

from werkzeug.utils import import_string
from ._compat import string_types


class ConfigAttribute(object):
    """Makes an attribute forward to the config"""
    # KY: 使一个属性转发到config配置.

    def __init__(self, name, get_converter=None):
        self.__name__ = name
        self.get_converter = get_converter

    def __get__(self, obj, type=None):
        if obj is None:
            return self
        rv = obj.config[self.__name__]
        if self.get_converter is not None:
            rv = self.get_converter(rv)
        return rv

    def __set__(self, obj, value):
        obj.config[self.__name__] = value


class Config(dict):
    """Works exactly like a dict but provides ways to fill it from files
    or special dictionaries.  There are two common patterns to populate the
    config.

    # KY: 工作非常像dict, 但是提供方法从文件或特殊的字典来填补它. 有两种常用的填充配置模式来.

    Either you can fill the config from a config file.
    # KY: 你也可以从一个配置文件里填充配置项.
    ::

        app.config.from_pyfile('yourconfig.cfg')

    Or alternatively you can define the configuration options in the
    module that calls :meth:`from_object` or provide an import path to
    a module that should be loaded.  It is also possible to tell it to
    use the same module and with that provide the configuration values
    just before the call.

    # KY: 或者, 另一种选择, 你可以在模块里定义些配置项, 以方法'from_object'调用, 或者提
    # 供应该加载模块的导入路径. 还有一种可能, 告诉它使用相同的模块和调用之前提供的配置值.
    ::

        DEBUG = True
        SECRET_KEY = 'development key'
        app.config.from_object(__name__)

    In both cases (loading from any Python file or loading from modules),
    only uppercase keys are added to the config.  This makes it possible to use
    lowercase values in the config file for temporary values that are not added
    to the config or to define the config keys in the same file that implements
    the application.

    # KY: 在这两种情况下(从任何Python文件加载或从模块加载), 只有大写键添加到配置中. 这使它
    # 可以使用小写值配置文件的临时值不添加到配置, 或者在同一个文件中定义配置键实现的应用程序.

    Probably the most interesting way to load configurations is from an
    environment variable pointing to a file.

    # KY: 可能最有趣的负载配置方式是从一个环境变量的指向文件.
    ::

        app.config.from_envvar('YOURAPPLICATION_SETTINGS')

    In this case before launching the application you have to set this
    environment variable to the file you want to use.  On Linux and OS X
    use the export statement.

    # KY: 在这种情况下, 在启动应用程序之前, 你必须将此环境变量设置为您想要使用的文件.
    # 在 Linux and OS X上用导出语句.
    ::

        export YOURAPPLICATION_SETTINGS='/path/to/config/file'

    On windows use `set` instead.

    :param root_path: path to which files are read relative from.  When the
                      config object is created by the application, this is
                      the application's :attr:`~flask.Flask.root_path`.
    :param defaults: an optional dictionary of default values.

    # KY: root_path: 读取文件相对路径, 当配置对象被应用程序创建时, 这是应用程序的属性 'flask.Flask.root_path'.
    # KY: defaults: 一个可选的默认值的字典.
    """

    def __init__(self, root_path, defaults=None):
        dict.__init__(self, defaults or {})
        self.root_path = root_path

    def from_envvar(self, variable_name, silent=False):
        """Loads a configuration from an environment variable pointing to
        a configuration file.  This is basically just a shortcut with nicer
        error messages for this line of code::

            app.config.from_pyfile(os.environ['YOURAPPLICATION_SETTINGS'])

        :param variable_name: name of the environment variable.
        :param silent: set to `True` if you want silent failure for missing files.
        :return: bool. `True` if able to load config, `False` otherwise.
        """
        # KY: 从一个指向一个配置文件的环境变量加载配置. 这基本上是只是一个快捷方式, 为让这行代码的错误消息更友好.
        # variable_name: 环境变量名.
        # silent: 如果你想为丢失的文件失败时静默, 设置为True.
        # 返回bool值, 载入配置为True, 否则为False.

        rv = os.environ.get(variable_name)
        if not rv:
            if silent:
                return False
            raise RuntimeError('The environment variable %r is not set '
                               'and as such configuration could not be '
                               'loaded.  Set this variable and make it '
                               'point to a configuration file' %
                               variable_name)
        return self.from_pyfile(rv, silent=silent)

    def from_pyfile(self, filename, silent=False):
        """Updates the values in the config from a Python file.  This function behaves
        as if the file was imported as module with the :meth:`from_object` function.

        :param filename: the filename of the config.  This can either be an
                         absolute filename or a filename relative to the root path.
        :param silent: set to `True` if you want silent failure for missing files.

        .. versionadded:: 0.7
           `silent` parameter.
        """
        # KY: 从一个python配置文件中更新值, 如果文件导入, 这个函数的行为, 如同 'from_object' function的模块.
        # filename: 配置文件名. 这可以是一个绝对文件名或相对于根路径的文件名.
        # silent: 如果你想为丢失的文件失败时静默, 设置为True.

        filename = os.path.join(self.root_path, filename)
        d = imp.new_module('config')
        d.__file__ = filename
        try:
            with open(filename) as config_file:
                exec (compile(config_file.read(), filename, 'exec'), d.__dict__)
        except IOError as e:
            if silent and e.errno in (errno.ENOENT, errno.EISDIR):
                return False
            e.strerror = 'Unable to load configuration file (%s)' % e.strerror
            raise
        self.from_object(d)
        return True

    def from_object(self, obj):
        """Updates the values from the given object.  An object can be of one
        of the following two types:

        # KY: 更新来自给定对象的值, 一个对象可以是以下两种类型之一.

        -   a string: in this case the object with that name will be imported.
        -   an actual object reference: that object is used directly.

        # KY: 一个字符串: 在这种情况下,对象的名称将进口.
        # KY: 一个实际引用对象: 直接使用的对象.

        Objects are usually either modules or classes.
        # KY: Objects 通常是模块或者类.

        Just the uppercase variables in that object are stored in the config.
        # KY: 大写的变量, 存储在配置的对象里.

        Example usage::

            app.config.from_object('yourapplication.default_config')
            from yourapplication import default_config
            app.config.from_object(default_config)

        You should not use this function to load the actual configuration but
        rather configuration defaults.  The actual config should be loaded
        with :meth:`from_pyfile` and ideally from a location not within the
        package because the package might be installed system wide.

        # KY: 你不应该使用这个函数来加载实际配置, 而是配置默认值. 实际的配置应该
        # 用 'from_pyfile' 加载, 来自本地的包未必理想, 因为这个包可能已安装在系统.

        :param obj: an import name or object.
        # KY: 一个导入的名称或对象.
        """
        if isinstance(obj, string_types):
            obj = import_string(obj)
        for key in dir(obj):
            if key.isupper():
                self[key] = getattr(obj, key)

    def __repr__(self):
        return '<%s %s>' % (self.__class__.__name__, dict.__repr__(self))
