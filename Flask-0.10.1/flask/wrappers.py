#!usr/bin/env python
# coding: utf-8

"""
    flask.wrappers
    ~~~~~~~~~~~~~~

    Implements the WSGI wrappers (request and response).

    :copyright: (c) 2011 by Armin Ronacher.
    :license: BSD, see LICENSE for more details.
"""
# KY: 实现了WSGI包装(请求和响应).

from werkzeug.wrappers import Request as RequestBase, Response as ResponseBase
from werkzeug.exceptions import BadRequest

from .debughelpers import attach_enctype_error_multidict
from . import json
from .globals import _request_ctx_stack


_missing = object()


def _get_data(req, cache):
    getter = getattr(req, 'get_data', None)
    if getter is not None:
        return getter(cache=cache)
    return req.data


class Request(RequestBase):
    """The request object used by default in Flask.  Remembers the
    matched endpoint and view arguments.

    It is what ends up as :class:`~flask.request`.  If you want to replace
    the request object used you can subclass this and set
    :attr:`~flask.Flask.request_class` to your subclass.

    The request object is a :class:`~werkzeug.wrappers.Request` subclass and
    provides all of the attributes Werkzeug defines plus a few Flask
    specific ones.
    """
    # KY: 在Flask中默认使用的请求对象, 记忆匹配端点和视图参数.

    # KY: 它是 'flask.request' 的最终. 如果你想用你的这个子类替代请
    # 求对象, 设置属性 'flask.Flask.request_class'为你的子类.

    # KY: 请求对象是一个 'werkzeug.wrappers.Request' 的子类, 并提供所有的
    # 属性 Werkzeug 定义, 附加一些Flask 特性.

    #: the internal URL rule that matched the request.  This can be
    #: useful to inspect which methods are allowed for the URL from
    #: a before/after handler (``request.url_rule.methods``) etc.
    #:
    #: .. versionadded:: 0.6

    # KY: 匹配请求的内部URL规则. 这会是有用的的检查, 哪个方法从URL前后处理程序被允许.
    url_rule = None

    #: a dict of view arguments that matched the request.  If an exception
    #: happened when matching, this will be `None`.

    # KY: 匹配请求的视图参数字典, 如果一个异常在匹配时发生, 它将是'None'.
    view_args = None

    #: if matching the URL failed, this is the exception that will be
    #: raised / was raised as part of the request handling.  This is
    #: usually a :exc:`~werkzeug.exceptions.NotFound` exception or
    #: something similar.

    # KY: 如果匹配URL失败, 这是异常,将会是:提高/长大作为请求的一部分处理.
    # 这通常是 'werkzeug.exceptions.NotFound' 异常或类似的东西.
    routing_exception = None

    # switched by the request context until 1.0 to opt in deprecated
    # module functionality

    # KY: 由请求上下文切换, 直到1.0 选择弃用模块功能.
    _is_old_module = False

    @property
    def max_content_length(self):
        """Read-only view of the `MAX_CONTENT_LENGTH` config key."""
        # KY: 只读 视图的 'MAX_CONTENT_LENGTH' 配置键.

        ctx = _request_ctx_stack.top
        if ctx is not None:
            return ctx.app.config['MAX_CONTENT_LENGTH']

    @property
    def endpoint(self):
        """The endpoint that matched the request.  This in combination with
        :attr:`view_args` can be used to reconstruct the same or a
        modified URL.  If an exception happened when matching, this will
        be `None`.
        """
        # KY: 匹配请求的端点, 这结合:attr:“视图args”可以用来重建相同或修改URL.
        # 如果匹配时发生异常, 这将会是'None'.

        if self.url_rule is not None:
            return self.url_rule.endpoint

    @property
    def module(self):
        """The name of the current module if the request was dispatched
        to an actual module.  This is deprecated functionality, use blueprints
        instead.
        """
        # KY: 当前模块的名称, 如果请求被分发到一个实际的模块.
        # 这是弃用功能, 已经用 blueprints 替代.

        from warnings import warn

        warn(DeprecationWarning('modules were deprecated in favor of '
                                'blueprints.  Use request.blueprint '
                                'instead.'), stacklevel=2)
        if self._is_old_module:
            return self.blueprint

    @property
    def blueprint(self):
        """The name of the current blueprint"""
        # KY: 当前模块的名称, 如果请求被分发到一个实际的模块.

        if self.url_rule and '.' in self.url_rule.endpoint:
            return self.url_rule.endpoint.rsplit('.', 1)[0]

    @property
    def json(self):
        """If the mimetype is `application/json` this will contain the
        parsed JSON data.  Otherwise this will be `None`.

        The :meth:`get_json` method should be used instead.
        """
        # KY: 如果mimetype是 'application/json' 这将包含一个被解析的JSON数据, 否则这将是'None'.

        # XXX: deprecate property
        return self.get_json()

    def get_json(self, force=False, silent=False, cache=True):
        """Parses the incoming JSON request data and returns it.  If
        parsing fails the :meth:`on_json_loading_failed` method on the
        request object will be invoked.  By default this function will
        only load the json data if the mimetype is ``application/json``
        but this can be overriden by the `force` parameter.

        :param force: if set to `True` the mimetype is ignored.
        :param silent: if set to `False` this method will fail silently
                       and return `False`.
        :param cache: if set to `True` the parsed JSON data is remembered
                      on the request.
        """
        # KY: 解析JSON请求数据并返回它, 如果解析失败, 请求对象的 'on_json_loading_failed' 方法
        # 将会被调用. 如果mimetype是 'application/json', 默认地, 这个函数仅仅载入JSON数据, 但
        # 这可以重载的 'force' 参数.

        # force: 如果设置为 'False', mimetype 被忽略.
        # silent: 如果设置为'False', 这个方法将静默地失败, 并返回'False'.
        # cache: 如果设置为'True', 解析JSON数据被记忆在请求时.

        rv = getattr(self, '_cached_json', _missing)
        if rv is not _missing:
            return rv

        if self.mimetype != 'application/json' and not force:
            return None

        # We accept a request charset against the specification as
        # certain clients have been using this in the past.  This
        # fits our general approach of being nice in what we accept
        # and strict in what we send out.

        # KY: 我们接受一个请求字符集以应对某些客户在过去一直使用的这个规范.
        # 这符合我们友善的一般方法, 也在我们接受和我们发出的严格之内.

        request_charset = self.mimetype_params.get('charset')
        try:
            data = _get_data(self, cache)
            if request_charset is not None:
                rv = json.loads(data, encoding=request_charset)
            else:
                rv = json.loads(data)
        except ValueError as e:
            if silent:
                rv = None
            else:
                rv = self.on_json_loading_failed(e)
        if cache:
            self._cached_json = rv
        return rv

    def on_json_loading_failed(self, e):
        """Called if decoding of the JSON data failed.  The return value of
        this method is used by :meth:`get_json` when an error occurred.  The
        default implementation just raises a :class:`BadRequest` exception.

        .. versionchanged:: 0.10
           Removed buggy previous behavior of generating a random JSON
           response.  If you want that behavior back you can trivially
           add it by subclassing.

        .. versionadded:: 0.8
        """
        # KY: 如果在decode JSON 数据时失败会被调用. 当错误发生时这个方法的返回值
        # 被'get_json'使用. 它默认实现仅仅抛出一个'BadRequest'异常.

        # KY: version changed 0.10
        # 删除之前生成的一个随机的JSON响应行为, 如果你想退回这一行为, 可以通过子类添加它.

        raise BadRequest()

    def _load_form_data(self):
        RequestBase._load_form_data(self)

        # in debug mode we're replacing the files multidict with an ad-hoc
        # subclass that raises a different error for key errors.

        # KY: 在调试模式中, 我们用一个ad-hoc子类替换文件 'multidict', 为key错误抛出一个不同的错误.
        ctx = _request_ctx_stack.top
        if ctx is not None and ctx.app.debug and \
                        self.mimetype != 'multipart/form-data' and not self.files:
            attach_enctype_error_multidict(self)


class Response(ResponseBase):
    """The response object that is used by default in Flask.  Works like the
    response object from Werkzeug but is set to have an HTML mimetype by
    default.  Quite often you don't have to create this object yourself because
    :meth:`~flask.Flask.make_response` will take care of that for you.

    If you want to replace the response object used you can subclass this and
    set :attr:`~flask.Flask.response_class` to your subclass.
    """
    # KY: 在Flask中默认情况下使用的响应对象. 工作方式像werkzeug的响应对象, 但是被默认设置
    # 为HTML mimetype. 通常你不需要自己创建这个对象,因为 'flask.Flask.make_response'
    # 会照顾你, 替你做出这些.

    # KY: 如果你想用你的子类替换这个响应对象, 设置 'flask.Flask.response_class' 为你的子类.

    default_mimetype = 'text/html'
