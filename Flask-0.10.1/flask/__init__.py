#!usr/bin/env python
# coding: utf-8

"""
    flask
    ~~~~~

    A microframework based on Werkzeug.  It's extensively documented
    and follows best practice patterns.

    :copyright: (c) 2011 by Armin Ronacher.
    :license: BSD, see LICENSE for more details.
"""
# KY: 基于Werkzeug的一个微框架, 它被广为记载,遵循最佳实践模式.

# KY: 版本定义常量.
__version__ = '0.10.1'

# utilities we import from Werkzeug and Jinja2 that are unused
# in the module but are exported as public interface.
# KY: 我们从Werkzeug 和Jinja2导入工具集, 它是未使用的模块, 但作为公共接口被调用.

from werkzeug.exceptions import abort
from werkzeug.utils import redirect
from jinja2 import Markup, escape

from .app import Flask, Request, Response
from .config import Config
from .helpers import (url_for, flash, send_file, send_from_directory, get_flashed_messages,
                      get_template_attribute, make_response, safe_join, stream_with_context)
from .globals import (current_app, g, request, session, _request_ctx_stack, _app_ctx_stack)
from .ctx import (has_request_context, has_app_context, after_this_request, copy_current_request_context)
from .module import Module
from .blueprints import Blueprint
from .templating import render_template, render_template_string

# the signals
from .signals import (signals_available, template_rendered, request_started, request_finished,
                      got_request_exception, request_tearing_down, appcontext_tearing_down,
                      appcontext_pushed, appcontext_popped, message_flashed)

# We're not exposing the actual json module but a convenient wrapper around it.
# KY: 我们不公开的实际json模块,但对它一个方便的包装.
from . import json

# This was the only thing that flask used to export at one point and it had a more generic name.
# KY: 这里唯一的事是flask用于在一个点调度, 让它有一个更通用的名字.
jsonify = json.jsonify

# backwards compat, goes away in 1.0
# KY: 向后兼容, 在1.0中消失滚蛋.
from .sessions import SecureCookieSession as Session

json_available = True
