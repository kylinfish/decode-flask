#!usr/bin/env python
# coding: utf-8

"""
    flask.ext
    ~~~~~~~~~

    Redirect imports for extensions.  This module basically makes it possible
    for us to transition from flaskext.foo to flask_foo without having to
    force all extensions to upgrade at the same time.

    When a user does ``from flask.ext.foo import bar`` it will attempt to
    import ``from flask_foo import bar`` first and when that fails it will
    try to import ``from flaskext.foo import bar``.

    We're switching from namespace packages because it was just too painful for
    everybody involved.

    :copyright: (c) 2011 by Armin Ronacher.
    :license: BSD, see LICENSE for more details.
"""

# KY: 为扩展重定向import使用, 这个模块主要使我们从 'flaskext.foo' 过渡 'flask_foo' 成为可能,
# 无需迫使所有扩展在同一时间升级. 当一个用户用 'from flask.ext.foo import bar', 它将优先试
# 图 'from flask_foo import bar', 如果失败它将再尝试　'from flask.ext.foo import bar'.
# 我们转换从名称空间的包, 因为它对涉及的每个人都太痛苦了.

# KY: Flask Before 0.8 之前的包里, 这个文件是不存在滴. 更多详情参看文档: extensions.html


def setup():
    from ..exthook import ExtensionImporter

    importer = ExtensionImporter(['flask_%s', 'flaskext.%s'], __name__)
    importer.install()


setup()
del setup
